import 'package:fibonacci/src/pages/home_page.dart';
import 'package:fibonacci/src/pages/home_page_stateful.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Center(child: ConteoPage()),
    );
  }
}
