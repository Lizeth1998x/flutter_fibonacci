import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ConteoPage extends StatefulWidget {
  @override
  _ConteoPage createState() => _ConteoPage();
}

class _ConteoPage extends State<ConteoPage> {
  int _contador = 0;
  int _n1 = 1;
  int _n2 = 0;
  int _resultado = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('página 1'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('El contador'),
            Text('es'),
            Text('$_contador'),
            Text('El número fibonacci es '),
            Text('$_resultado'),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          setState(() {
            _contador++;
            _resultado = _n1 + _n2;
            _n1 = _n2;
            _n2 = _resultado;
          });
        },
      ),
    );
  }
}
